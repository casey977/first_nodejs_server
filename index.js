'use strict';

const http = require('http');
const fs = require('fs');

const hostname = '127.0.0.1';
const port = 3333;
const months = ['januar', 'februar', 'marts', 'april', 'maj', 'juni', 'juli', 'august', 'september', 'oktober', 'november', 'december'];

const le_style = fs.readFileSync('style.css', {'encoding': 'utf8', flag: 'r'});

const server = http.createServer();

server.on('request', server_func);

server.listen(port, hostname, listen_func);

// Functions...

function server_func(req, res)
	{
		const moment = new Date();
		const year = moment.getFullYear();
		const month = moment.getMonth();
		const day = moment.getDate();
		const hour = String(moment.getHours()).padStart(2, '0');
		const minutes = String(moment.getMinutes()).padStart(2, '0');
		const le_time = `${hour}:${minutes}`;
		const le_date = `${day}. ${months[month]}, ${year}`;

		if(req.url !== '/style.css')
			{
				res.writeHead(200, {'Content-Type': 'text/html'});
				res.end(`<html><head><link rel="stylesheet" href="style.css"></head><body><p>${le_time}<br>${le_date}</p></body></html>`);
			}
		else
			{
				res.writeHead(200, {'Content-Type': 'text/css'});
				res.end(le_style);
			}
	}

function listen_func()
	{
		console.log(`Server running and listening to port ${port} at ${hostname}.`);
	}