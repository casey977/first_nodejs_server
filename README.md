This project is merely an exercise in vanilla NodeJS (without using express, etc.).

It should not be understood as an attempt to follow best NodeJS practices.

It should not be understood as an attempt to make a RESTful design either.